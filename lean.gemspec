# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "lean"
  spec.version       = "0.1.0"
  spec.authors       = ["lafleur"]
  spec.email         = ["lafleur@boum.org"]

  spec.summary       = "a lean layout using masonry"
  spec.homepage      = "http://lafleurdeboum.gitlab.io/lean"
  spec.license       = "GPL"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.1"
end
