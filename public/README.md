# lean


## Installation

```
git clone https://gitlab.com/lafleurdeboum/lean-jekyll-theme
cd lean-jekyll-theme
bundle exec jekyll serve
```

Obviously you need `git`, ruby's `bundle` and `jekyll`. If you don't have all
of this, you can fork this project in gitlab (you will need a possibly free
account). The included CI will build the `sample` branch and make it visible at
`https://YOURUSERNAME.gitlab.io/YOURFORKNAME`.

## Usage

Significant files include :
  - `_layouts/default.html` has the main layout, with a wrapper containing
  header, section and footer.
  - `_sass/lean.scss` does the styling. Mostly, a flexbox row inside a flexbox
  column. Comes with an optional "main categories" menu presenting pages bearing
  `is-main: true`.
  - `assets/style/fonts.css` get lazy-loaded at the end of `default.html`.
  - `.gitlab-ci.yml` contains basic instructions for a cached build of the
  site in the `sample` branch.

Removable files would be in the `sample` branch :
  - `_config.yml`, together with `index.md`, `about.md` and `main_cat/*`
  showcase a sample site that you can run with `bundle exec jekyll serve` –
  this theme is setup like a Jekyll site.

## Contributing

This project is intended to be a welcoming space for collaboration. If you have
an idea, suggestion, feature request, etc., feel free to open an issue or pull
request.

For bug reports, follow the provided template.

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, `_sass` and `assets` tracked with Git will be bundled.
To add a custom directory to your theme-gem, please edit the regexp in `lean.gemspec` accordingly.

## License

The theme is available as open source under the terms of the [GPL3 License](https://opensource.org/licenses/GPL-3.0).

